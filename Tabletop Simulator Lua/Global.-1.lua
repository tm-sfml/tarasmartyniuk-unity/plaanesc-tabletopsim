-- Global.lua
creature_definitions = nil
creature_prefab_guid = '9f5c95'
creature_place_marker_guid = '36f16a'
creatures_zone_guid = '5fc9a7'

function onLoad(save_state)
    creature_definitions = JSON.decode(save_state)
end

function on_figurine_spawned(figurine, definition)
    print('spawned: ', figurine.getName())
    figurine.call('setStats', definition)
    figurine.call('start')
end

function onSpawnCreaturesButtonClick()
    local creature_zone = getObjectFromGUID(creatures_zone_guid)
    
    for _, object in pairs(creature_zone.getObjects()) do
        if (object.tag == 'Tileset') then 
            destroyObject(object)
        end
    end

    local prefab = getObjectFromGUID(creature_prefab_guid)

    spawnOrigin = getObjectFromGUID(creature_place_marker_guid).getPosition()
    max_width = 2
    for i, definition in ipairs(creature_definitions) do
        offset = i - 1
        x = offset % max_width
        y = math.floor(offset / max_width)
        local spawned = spawnObjectJSON({
            json = prefab.getJSON(),
            position = spawnOrigin + Vector(x, 0, y),
            callback_function = function(obj) on_figurine_spawned(obj, definition) end
        })
    end
end