stats = {}
-- separate from stats since it changes
health = 0

function start()
    print('start')
    print(stats)
    local descr = 'Movement:' .. stats['Movement'] .. '\n' ..
    'Range: ' .. stats['Range'] .. '\n' ..
    'ATK/HP: ' .. getStatLabel()

    self.setName(stats['DefinitionId'])
    self.setDescription(descr)

    local rotation = {60, 180, 0}
    local params = {
        click_function = "empty_func",
        function_owner = self,
        label          = self.getName(),
        position       = {0, 0.5, -0.5},
        rotation       = rotation,
        width          = 900,
        height         = 60,
        font_size      = 110,
    }
    self.createButton(params)
    
    stat_button_position = Vector(0.3, 0, -1)
    params = {
        click_function = "on_stat_label_click",
        function_owner = self,
        label          = getStatLabel(),
        position       = stat_button_position,
        rotation       = rotation,
        width          = 900,
        height         = 400,
        font_size      = 250,
    }
    self.createButton(params)

    params.click_function = "on_reset_stats_click"
    stat_button_position.x = -1
    params.position = stat_button_position
    params.label = ''
    params.width = 300
    params.color = {1, 0, 0}
    self.createButton(params)

end

function setStats(stats_definition)
    stats = stats_definition
    health = stats['Health']
end

function getStatLabel()
    return stats['Attack'] .. '/' .. health
end


function on_stat_label_click(obj, color, alt_click)
    health = health - 1
    self.editButton({index=1, label=getStatLabel()})
end

function on_reset_stats_click(obj, color, alt_click)
    health = stats['Health']
    self.editButton({index=1, label=getStatLabel()})
end

function empty_func()  end