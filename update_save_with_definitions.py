import json

ProjectRoot = 'C:/dev/unity/planeswalker_escape'
CardDefinitionsFilename = f'{ProjectRoot}/Assets/StreamingAssets/creature_definitions.json'
TabletopSimSaveRoot = 'C:/Users/taras/Documents/My Games/Tabletop Simulator/Saves'
TabletopSimSaveFile = f'{TabletopSimSaveRoot}/FirstProj/TS_Save_1.json'

card_defs_json = None
with open(CardDefinitionsFilename, 'r') as file:
    card_defs_json = file.read()

with open(TabletopSimSaveFile, 'r+') as file:
    save_json = json.loads(file.read())
    file.seek(0)
    save_json["LuaScriptState"] = card_defs_json
    file.write(json.dumps(save_json, indent=4, ))
    file.truncate()


print(card_defs_json)
